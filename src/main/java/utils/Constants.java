package utils;

public class Constants {

    public static final String CHROME = "Chrome";
    public static final String FIREFOX = "Firefox";
    public static final String URL = "https://www.booking.com/";
    public static final String PARIS = "Paris";
    public static final String OCT21 = "October 2021";
    public static final String NUMOFADULTS = "2";
    public static final String NUMOFCHLD = "1";
    public static final String NUMOFROOMS = "1";
    public static final String CHECKINDATE = "2021-10-19";
    public static final String CHECKOUTDATE = "2021-10-25";
    public static final String CHECKOUTDATE31 = "2021-11-19";
    public static final int TIMEOUT = 10;
    public static final String HOTELRGX = "Novotel\\s+Paris\\s+Les\\s+Halles";
    public static final String HOTEL = "Novotel Paris Les Halles";
    public static final String ERRORMSGDEST = "Enter a destination to start searching.";
    public static final String ERRORMSGDATE = "Reservations longer than 30 nights are not possible.";

}
