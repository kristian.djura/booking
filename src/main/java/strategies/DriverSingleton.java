package strategies;

import drivers.Chrome;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

//This class makes only one instance of driver
public class DriverSingleton {

    private static DriverSingleton instance = null;
    private static WebDriver driver;

    private DriverSingleton(String driver){

        instantiate(driver);
    }

    //Method that instantiets webdrive that is passed, in this project, I am only passing Chrome
    public WebDriver instantiate(String webdriver) {

        DriverStrategy driverStrategy = ChoseWebDriver.choseWebDriver(webdriver);
        driver = driverStrategy.setStrategy();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        return driver;
    }

    public static DriverSingleton getInstance(String driver){

        if (instance==null){
            instance = new DriverSingleton(driver);
        }

        return instance;
    }

    public static void  closeObjectInstance(){

        instance = null;
        driver.quit();
    }

    public static WebDriver getDriver(){

        return driver;
    }
}
