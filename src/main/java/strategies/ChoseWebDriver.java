package strategies;

import drivers.Chrome;
import utils.Constants;

public class ChoseWebDriver {

    public static DriverStrategy choseWebDriver(String strategy) {

        switch (strategy) {
            case Constants.CHROME:
                return new Chrome();

            default:
                return null;
        }
    }
}
