package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import strategies.DriverSingleton;
import utils.Constants;

public class HomePage {

        private WebDriver driver;
        //Here are all elements from Home Page, that need to be lococated, also methods
        public HomePage(){

            this.driver = DriverSingleton.getDriver();
            PageFactory.initElements(driver, this);
        }

        //Web Element where the destination is inserted
        @FindBy(id = "ss")
        private WebElement searchCity;

        //Web Element in which the dates are entered by clicking on specific dates
        @FindBy(css = "#frm > div.xp__fieldset.js--sb-fieldset.accommodation > div.xp__dates.xp__group > div.xp__dates-inner > div:nth-child(2) > div > div > div > div")
        private WebElement checkInCheckOut;

        //Web Element where the current month and year are located, after clicking on checkInCheckOut button is clicked
        @FindBy(xpath = "/html/body/div[2]/div/div/div[2]/form/div[1]/div[2]/div[2]/div/div/div[3]/div[1]/div")
        private WebElement currentMonth;

        //Web Element in calendar where months could be changed by clicking on it
        @FindBy(css = "#frm > div.xp__fieldset.js--sb-fieldset.accommodation > div.xp__dates.xp__group > div.xp-calendar > div > div > div.bui-calendar__control.bui-calendar__control--next > svg")
        private WebElement arrowFindMonth;

//        @FindBy(css = "#frm > div.xp__fieldset.js--sb-fieldset.accommodation > div.xp__dates.xp__group > div.xp-calendar > div > div > div.bui-calendar__content > div:nth-child(1) > table > tbody > tr:nth-child(4) > td:nth-child(2)")
//        private WebElement checkInDate;
//
//        @FindBy(css = "#frm > div.xp__fieldset.js--sb-fieldset.accommodation > div.xp__dates.xp__group > div.xp-calendar > div > div > div.bui-calendar__content > div:nth-child(1) > table > tbody > tr:nth-child(5) > td:nth-child(1)")
//        private WebElement checkOutDate;

        //Relative xpath to Web Element, so specific check in date could be clicked, which is located in Constants
        @FindBy(xpath = "//*[@data-date='" +Constants.CHECKINDATE+ "']")
        private WebElement checkInDate;

        //Relative xpath to Web Element, so specific check out date could be clicked, which is located in Constants
        @FindBy(xpath = "//*[@data-date='" +Constants.CHECKOUTDATE+ "']")
        private WebElement checkOutDate;

        //Relative xpath to Web Element, so negative test could be executed, 31 days from check in date
        @FindBy(xpath = "//*[@data-date='" +Constants.CHECKOUTDATE31+ "']")
        private WebElement checkOutDate31Night;

        //Web Element where accomondation settings could be set
        @FindBy(id = "xp__guests__toggle")
        private WebElement numberOfGuests;

        //Web Element of default vaule of adults
        @FindBy(css = "#xp__guests__inputs-container > div > div > div.sb-group__field.sb-group__field-adults > div > div.bui-stepper__wrapper.sb-group__stepper-a11y > span.bui-stepper__display")
        private WebElement numberOfAdults;

        //Default value od children
        @FindBy(css = "#xp__guests__inputs-container > div > div > div.sb-group__field.sb-group-children > div > div.bui-stepper__wrapper.sb-group__stepper-a11y > span.bui-stepper__display")
        private WebElement numberOfChildren;

        //Default valus of rooms
        @FindBy(css = "#xp__guests__inputs-container > div > div > div.sb-group__field.sb-group__field-rooms > div > div.bui-stepper__wrapper.sb-group__stepper-a11y > span.bui-stepper__display")
        private WebElement numberOfRooms;

        //Button for reducing number of adults
        @FindBy(css = "#xp__guests__inputs-container > div > div > div.sb-group__field.sb-group__field-adults > div > div.bui-stepper__wrapper.sb-group__stepper-a11y > button.bui-button.bui-button--secondary.bui-stepper__subtract-button")
        private WebElement minusAdults;

        //Button for adding number of adults
        @FindBy(css = "#xp__guests__inputs-container > div > div > div.sb-group__field.sb-group__field-adults > div > div.bui-stepper__wrapper.sb-group__stepper-a11y > button.bui-button.bui-button--secondary.bui-stepper__add-button")
        private WebElement plusAdults;

        //Button for reducing number od children
        @FindBy(css = "#xp__guests__inputs-container > div > div > div.sb-group__field.sb-group-children > div > div.bui-stepper__wrapper.sb-group__stepper-a11y > button.bui-button.bui-button--secondary.bui-stepper__subtract-button.sb-group__stepper-button-disabled")
        private WebElement minusChildren;

        //Button for adding number of children
        @FindBy(css = "#xp__guests__inputs-container > div > div > div.sb-group__field.sb-group-children > div > div.bui-stepper__wrapper.sb-group__stepper-a11y > button.bui-button.bui-button--secondary.bui-stepper__add-button")
        private WebElement plusChildren;

        //Button for reducing number of rooms
        @FindBy(css = "#xp__guests__inputs-container > div > div > div.sb-group__field.sb-group__field-rooms > div > div.bui-stepper__wrapper.sb-group__stepper-a11y > button.bui-button.bui-button--secondary.bui-stepper__subtract-button.sb-group__stepper-button-disabled")
        private WebElement minusRooms;

        //Button for adding number of rooms
        @FindBy(css = "#xp__guests__inputs-container > div > div > div.sb-group__field.sb-group__field-rooms > div > div.bui-stepper__wrapper.sb-group__stepper-a11y > button.bui-button.bui-button--secondary.bui-stepper__add-button")
        private WebElement plusRooms;

        //Web Element of search button
        @FindBy(css = "#frm > div.xp__fieldset.js--sb-fieldset.accommodation > div.xp__button > div.sb-searchbox-submit-col.-submit-button > button")
        private WebElement searchButton;

        //Error message for not entering destination value
        @FindBy(css = "#destination__error")
        private WebElement errorMessageDest;

        //Error message for trying to book 31 nights
        @FindBy(css = "#frm > div.xp__fieldset.js--sb-fieldset.accommodation > div.xp__dates.xp__group > div.xp__dates-inner > div:nth-child(1) > div > div")
        private WebElement errorMessageDate;

        //Method that enters destination in field
        public void enterDestination(String city){

                searchCity.sendKeys(city);
        }

        //Method that clicks on field for setting the dates
        public void setCheckInChekOutDate(String month) {

                checkInCheckOut.click();

                //This loop click arrow for next month, until the specific month is found
                while (!currentMonth.getText().equals(month)){

                        arrowFindMonth.click();

                }
                //After the match is true, check in and check out dates are entered
                checkInDate.click();
                checkOutDate.click();
        }

        //Method for testing negative scenario, where 31 days are selected
        public void setCheckInChekOutDateFor31Night(String month) {

                checkInCheckOut.click();

                while (!currentMonth.getText().equals(month)){

                        arrowFindMonth.click();

                }

                checkInDate.click();
                checkOutDate31Night.click();

        }


        //Method for setting number of adults, children, and rooms
        public void setGuests(String adults, String children, String rooms){

                numberOfGuests.click();

                while(!numberOfAdults.getText().equals(adults)){

                        plusAdults.click();
                }
                while(!numberOfChildren.getText().equals(children)){

                        plusChildren.click();
                }
                while(!numberOfRooms.getText().equals(rooms)){

                        plusRooms.click();
                }

                searchButton.click();

        }

        //Method for comparing the message for not entering destination
        public boolean inspectingErrorMessageDest(){

                return errorMessageDest.getText().contains(Constants.ERRORMSGDEST);

        }

        //Method for comparing the message entering 31 day booking
        public boolean inspectingErrorMessageDate(){

                return errorMessageDate.getText().contains(Constants.ERRORMSGDATE);
        }

        public void clearTextFromDestinationBar(){

                searchCity.clear();
        }

}