package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import strategies.DriverSingleton;
import utils.Constants;

import java.time.Duration;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ResultPage {

    private WebDriver driver;

    //Web Page which is presented after all fields are correctly entered on Home Page. On this page all hotels for entered destination, and date range are displayed
    public ResultPage(){

        this.driver = DriverSingleton.getDriver();
        PageFactory.initElements(driver, this);
    }

    //Web Element, arrow in the bottom of the page which iterates list of hotels to next page
    @FindBy(css = "#search_results_table > div.bui-pagination.results-paging_simplified.js-results-paging > nav > ul > li.bui-pagination__item.bui-pagination__next-arrow > a > svg")
    private WebElement nextPage;

    @FindBy(xpath = "/html/body/div[3]/div/div[3]/div[1]/div[1]/div[4]/div[5]/div[1]/div/div[1]")
    private WebElement hotelNovotelParisLesHalles;

    //Method that goes through list of all hotels, until finds specific hotel
    public void findingHotel() {

        WebDriverWait wait=new WebDriverWait(driver, 10);

        //Matcher that checks if specific hotel is present on the page, list of hotels displayed on current page
        Matcher m = Pattern.compile(Constants.HOTELRGX).matcher(driver.getPageSource());

        while (!m.find())  {

            //Web Element that needs to be instantiated every time the page is iterated, so stalnessOf method could be used
            WebElement hotelList = driver.findElement(By.id("hotellist_inner"));
            nextPage.click();
            wait.until(ExpectedConditions.stalenessOf(hotelList));

            m = Pattern.compile(Constants.HOTELRGX).matcher(driver.getPageSource());
        }

        //When specific hotel is found, its located and clicked
        WebElement matchedHotel =driver.findElement(By.partialLinkText(Constants.HOTEL));
        matchedHotel.click();

    }

    //Method that switches to new tab that is opened when specific hotel is clicked
    public void switchToNewTab(){

        ArrayList<String> newTab = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(newTab.get(1));
    }

    public boolean checkForHotel(){

        return hotelNovotelParisLesHalles.getText().contains(Constants.HOTEL);
    }


}
