package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import strategies.DriverSingleton;
import utils.Constants;

//Purpose of this class method is to check if the right hotel is clicked
public class HotelPage {

    private WebDriver driver;

    public HotelPage(){

        this.driver = DriverSingleton.getDriver();
        PageFactory.initElements(driver,this);
    }

    @FindBy(css = "#frm > div.sb-searchbox__row.u-clearfix.-submit.sb-searchbox__footer.-last > div.sb-searchbox-submit-col.-submit-button > button")
    private WebElement button;

    @FindBy(css = "#hp_hotel_name")
    private WebElement hotelName;



    public boolean inspectingHotelName(){

        WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);
        wait.until(ExpectedConditions.elementToBeClickable(button));

        return hotelName.getText().contains(Constants.HOTEL);
    }


}
