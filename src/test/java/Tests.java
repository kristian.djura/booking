import org.junit.*;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.WebDriver;
import pages.HomePage;
import pages.HotelPage;
import pages.ResultPage;
import strategies.DriverSingleton;
import utils.Constants;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Tests {

    static HomePage homePage;
    static WebDriver driver;
    static ResultPage resultPage;
    static HotelPage hotelPage;

//    //Method that is executed once and before all Tests
//    @BeforeClass
//    public static void initializeObjects(){
//
//        DriverSingleton.getInstance(Constants.CHROME);
//        driver = DriverSingleton.getDriver();
//        homePage = new HomePage();
//        resultPage = new ResultPage();
//        hotelPage = new HotelPage();
//    }

    @Before
    public void initDriver(){

        DriverSingleton.getInstance(Constants.CHROME);
        driver = DriverSingleton.getDriver();
        homePage = new HomePage();
        resultPage = new ResultPage();
        hotelPage = new HotelPage();
    }

    //Looking for specific hotel, after entering valid values(destination and dates), and going through pages to find it
    @Test
    public void test01LookingForNovotelParisLesHalles() throws InterruptedException {

        driver.get(Constants.URL);
        homePage.clearTextFromDestinationBar();
        homePage.enterDestination(Constants.PARIS);
        homePage.setCheckInChekOutDate(Constants.OCT21);
        homePage.setGuests(Constants.NUMOFADULTS, Constants.NUMOFCHLD, Constants.NUMOFROOMS);
        resultPage.findingHotel();
        resultPage.switchToNewTab();

        Assert.assertEquals(hotelPage.inspectingHotelName(), true);
    }

    //Entering the name of hotel in search to find it immediately
    @Test
    public void test02SearchingForNovotelParisLesHalles(){

        driver.get(Constants.URL);
        homePage.enterDestination(Constants.PARIS);
        homePage.enterDestination(" " +Constants.HOTEL);
        homePage.setCheckInChekOutDate(Constants.OCT21);
        homePage.setGuests(Constants.NUMOFADULTS, Constants.NUMOFCHLD, Constants.NUMOFROOMS);

        Assert.assertEquals(resultPage.checkForHotel(), true);

    }

    //Negative test, to test how app responds when more than 30 days is entered
    @Test
    public void test03ReservationFor31Night(){

        driver.get(Constants.URL);
        homePage.enterDestination(Constants.PARIS);
        homePage.setCheckInChekOutDateFor31Night(Constants.OCT21);
        homePage.setGuests(Constants.NUMOFADULTS, Constants.NUMOFCHLD, Constants.NUMOFROOMS);

        Assert.assertEquals(homePage.inspectingErrorMessageDate(), true);
    }

    //Negative test, to test how app responds when no destination is entered
    @Test
    public void test04NegativeTestNoDestination(){

        driver.get(Constants.URL);
        homePage.setCheckInChekOutDate(Constants.OCT21);
        homePage.setGuests(Constants.NUMOFADULTS, Constants.NUMOFCHLD, Constants.NUMOFROOMS);

        Assert.assertEquals(homePage.inspectingErrorMessageDest(), true);
    }



    @After
    public void closeDriver(){

        DriverSingleton.closeObjectInstance();
    }

//    //Method that is executed after all Tests
//    @AfterClass
//    public static void closeObjects(){
//
//        DriverSingleton.closeObjectInstance();
//    }
}
